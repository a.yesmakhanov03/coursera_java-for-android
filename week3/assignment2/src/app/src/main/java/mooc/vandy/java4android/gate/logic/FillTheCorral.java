package mooc.vandy.java4android.gate.logic;

import java.util.Random;

import mooc.vandy.java4android.gate.ui.OutputInterface;

/**
 * This class uses your Gate class to fill the corral with snails.  We
 * have supplied you will the code necessary to execute as an app.
 * You must fill in the missing logic below.
 */
public class FillTheCorral {
    /**
     * Reference to the OutputInterface.
     */
    private OutputInterface mOut;

    /**
     * Constructor initializes the field.
     */
    public FillTheCorral(OutputInterface out) {
        this.mOut = out;
    }


    // TODO -- Fill your code in here

    public void setCorralGates(Gate[] gates, Random selectDirection) {
        for (Gate g : gates) {
            g.setSwing(selectDirection.nextInt(3) - 1);
        }
    }

    public boolean anyCorralAvailable(Gate[] corral) {

        boolean hasNoInGate = true;
        for (Gate g : corral) {
            hasNoInGate &= g.getSwingDirection() != Gate.IN;
        }
        return !hasNoInGate;
    }

    public int corralSnails(Gate[] corral, Random rand) {
        int s, randomGateIndex;
        int attempts = 0;
        int out = 5;

        while (out <= HerdManager.HERD) {
            s = rand.nextInt(out);
            randomGateIndex = rand.nextInt(corral.length);
            out +=corral[randomGateIndex].thru(s);
            mOut.println(String.format("%d are trying to move through corral %d", s, randomGateIndex));
            attempts++;
        }
        return attempts;

    }
}
