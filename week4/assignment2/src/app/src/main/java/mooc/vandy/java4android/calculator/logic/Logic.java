package mooc.vandy.java4android.calculator.logic;

import java.util.Map;

import mooc.vandy.java4android.calculator.ui.ActivityInterface;

/**
 * Performs an operation selected by the user.
 */
public class Logic implements LogicInterface {
    /**
     * Reference to the Activity output.
     */
    protected ActivityInterface mOut;

    private Map<Integer, MathOperation> mAvailableOperations;

    /**
     * Constructor initializes the fields
     *
     * @param out                 - where the output of the logic will be printed out.
     * @param availableOperations - list of all supported operations at the moment. Just add next operation to this
     *                            Map, a write new class with math. operation.
     * @see mooc.vandy.java4android.calculator.ui.MainActivity
     */
    public Logic(ActivityInterface out, Map<Integer, MathOperation> availableOperations) {
        mOut = out;
        mAvailableOperations = availableOperations;
    }

    /**
     * Perform the @a operation on @a argumentOne and @a argumentTwo.
     * If the given @a operation is not supported the method print out "Unknown operation" message.
     *
     * @param argumentOne - first number
     * @param argumentTwo - second number
     * @param operation   - index of the operation from the Dropdown box in UI. Corresponding to the Key of the @mAvailableOperations
     */
    public void process(int argumentOne, int argumentTwo, int operation) {

        MathOperation mathOperation = mAvailableOperations.get(operation);
        if (mathOperation != null) {
            mOut.print(mathOperation.calc(argumentOne, argumentTwo));
        } else {
            mOut.print("Unknown operation");
        }
    }
}
