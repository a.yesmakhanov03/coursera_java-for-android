package mooc.vandy.java4android.calculator.logic;


/**
 * Helper class. Keeps operation ids.
 */
public class Operation {

    private Operation() {}

    public static final int ADD      = 1;
    public static final int SUBTRACT = 2;
    public static final int MULTIPLY = 3;
    public static final int DIVIDE   = 4;
}
